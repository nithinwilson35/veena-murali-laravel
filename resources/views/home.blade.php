@extends('layouts.admin')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-tasks bg-primary p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-primary">{{\App\Models\Category::active()->count()}}</div>
                    <div class="text-muted text-uppercase font-weight-bold small">Categories</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{ route('admin.categories.index') }}">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-box-open bg-success p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-success">{{\App\Models\Product::active()->count()}}</div>
                    <div class="text-muted text-uppercase font-weight-bold small">Products</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{ route('admin.products.index') }}">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-images bg-warning p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-warning">{{\App\Models\Banner::active()->count()}}</div>
                    <div class="text-muted text-uppercase font-weight-bold small">Banners</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{ route('admin.banners.index') }}">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-book-open bg-danger p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-danger">{{\App\Models\Page::count()}}</div>
                    <div class="text-muted text-uppercase font-weight-bold small">Pages</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="{{ route('admin.pages.index') }}">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-group mb-4">
        <div class="card">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="text-success fas fa-envelope"></i>
                </div>
                <div class="text-value text-success">{{\App\Models\Enquiry::count()}}</div>
                <small class="text-muted clr-def text-uppercase font-weight-bold">Enquiries</small>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="text-primary fas fa-envelope-square"></i>
                </div>
                <div class="text-value text-primary">{{\App\Models\ProductEnquiry::count()}}</div>
                <small class="text-muted clr-def text-uppercase font-weight-bold">Product Enquiries</small>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection