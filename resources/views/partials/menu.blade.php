<div class="sidebar">
    <nav class="sidebar-nav">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            @can('users_manage')
               <!-- <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle" href="#">
                        <i class="fa-fw fas fa-users nav-icon">

                        </i>
                        {{ trans('cruds.userManagement.title') }}
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-unlock-alt nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-briefcase nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <i class="fa-fw fas fa-user nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    </ul>
                </li>-->
            @endcan
            <li class="nav-item">
                <a href="{{ route('admin.categories.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-tasks"></i>
                    {{ trans('cruds.category.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.products.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-box-open"></i>
                    {{ trans('cruds.product.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.banners.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-images"></i>
                    {{ trans('cruds.banner.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.pages.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-book-open"></i>
                    {{ trans('cruds.page.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.enquiries.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-envelope"></i>
                    {{ trans('cruds.enquiry.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.product-enquiries.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-envelope-square"></i>
                    {{ trans('cruds.product_enquiry.title') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.review.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-pencil-alt"></i>
                    Review
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('auth.change_password') }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-key">

                    </i>
                    Change password
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-fw fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
        </ul>

    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>