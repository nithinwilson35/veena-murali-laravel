<!doctype html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><!-- Required meta tags --><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><meta name="theme-color" content="#ffa515" /><!-- Bootstrap CSS -->
    <link href="{{asset('themes/comingsoon/css/style.css')}}" rel="stylesheet" />
    <title>Veena Murali Decors</title>
</head>
<body autocomplete="off">
<section class="wrapper">
<div class="left-image"></div>

<div class="content-area">
<div class="kathu">
<div class="center">
<div class="veena-logo"><img alt="" src="{{asset('themes/comingsoon/images/logo.jpg')}}" /></div>

<p>Something like a curated showcase for antique south indian curios and contemporary gift ideas are about to launch here.<br />
<br />
Soon, very very soon.<br />
Stay tuned.</p>

<div class="social"><a class="fb" href="https://www.facebook.com/Veenamuralidecors/"><img alt="" src="{{asset('themes/comingsoon/images/facebook.jpg')}}" /></a> <a class="insta" href="https://www.instagram.com/veenamuralidecors/"><img alt="" src="{{asset('themes/comingsoon/images/instagram.jpg')}}" /></a></div>
</div>
</div>
</div>
</section>

<div class="hide-000logo"></div>
</body>
</html>