@extends('layouts.frontend')

@section('content')
<section class="inner-banner">
          <div class="img-box">
            <img src="{{url('storage/app/'.$seo->image)}}" alt="">
          </div>
          <div class="text-box">
              <h2>Contact Us</h2>
            </div>
        </section>
        <section class="contact-area">
            <div class="container-fluid">
                <div class="left-area">
                  <h2>You've Got Questions, <br> We've Got Answers</h2>
                  <form id="contact-form" action="{{url('/contact-us')}}" method="post">
                          @csrf
                      <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-box">
                                <input name="name" required='required'>
                                <lable>
                                  Name <span class="text-danger font-weight-bold">*</span>
                                </lable>
                              </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-box">
                                <input name="phone" required='required'>
                                <lable>
                                    Phone Number <span class="text-danger font-weight-bold">*</span>
                                </lable>
                              </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-box">
                                <input name="email" >
                                <lable>
                                  Email
                                </lable>
                              </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-box">
                                    <textarea name="message" ></textarea>
                                <lable>
                                    Message 
                                </lable>
                              </div>
                        </div>
                        <div class="col-sm-12">
                                <button id="contact_submit">Submit Feedback <i><img src="{{asset('themes/frontend/assets/images/right.svg')}}" alt=""></i></button>
                        </div>
                        <div class="col-sm-12" id="msg_div" style="display:none;">
                            
                                <p style="margin-top:15px; color: #7cb342;font-weight: 600;font-size: 16px;">Message Submitted Successfully.</p>
                        </div>
                        <div class="col-sm-12" id="error_div" style="display:none;">
                        <p style="margin-top:15px; color: #cc0000;font-weight: 600;font-size: 16px;">Fill the form before submitting.</p>
                        </div>
                      </div>
                  </form>
                </div>
                <div class="right-area">
                  <img src="{{asset('themes/frontend/assets/images/call.png')}}" class="telephone">
                    <ul>
                      <!--<li class="address">
                          DieSachbearbeli <br>
                          Choriner Strabe 49 <br>
                          10435 Berlin <br>
                      </li>-->
                       <li class="email"><a href="mailto:veenamuralidecors2017@gmail.com">veenamuralidecors2017@gmail.com</a></li>
                       <li class="number">
                           <a href="tel:+916385141412">+91 6385141412</a>
                    </ul>
                  </div>
            </div>

        </section>






    <section class="bottom-text">
      {!! $seo->content !!}
    </section>
@endsection
@section('scripts')
<script>
$(function () {
      $("#contact_submit").on('click', function(e){
        $("#msg_div").hide();
        $("#error_div").hide();
        e.preventDefault();
        if (!$("#contact-form")[0].checkValidity || $("#contact-form")[0].checkValidity()) {
          var data = $("#contact-form").serialize();
          $.ajax({
              type: "POST",
              url: $("#contact-form").attr('action'),
              data:  data,
              beforeSend: function() {
                $("#contact-form")[0].reset();
                $("#msg_div").show('slow');
              },
              success: function(response) {
                  if(response.status) {
                  }
              }
          });
        } else {
            $("#error_div").show('slow');
        }
      })
    });
 </script>
@endsection