@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.banner.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.banners.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.banner.fields.title') }}*</label>
                            <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($banner) ? $banner->title : '') }}" required>
                            @if($errors->has('title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.banner.fields.title_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            <label for="status">{{ trans('cruds.banner.fields.status') }}*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            @if($errors->has('status'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.banner.fields.status_helper') }}
                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('link_text') ? 'has-error' : '' }}">
                            <label for="link_text">{{ trans('cruds.banner.fields.link_text') }}</label>
                            <input type="text" id="link_text" name="link_text" class="form-control" value="{{ old('link_text', isset($banner) ? $banner->link_text : '') }}">
                            @if($errors->has('heading'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('link_text') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.banner.fields.link_text_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('link_url') ? 'has-error' : '' }}">
                            <label for="link_url">{{ trans('cruds.banner.fields.link_url') }}</label>
                            <input type="text" id="link_url" name="link_url" class="form-control" value="{{ old('link_url', isset($banner) ? $banner->link_url : '') }}">
                            @if($errors->has('heading'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('link_url') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.banner.fields.link_url_helper') }}
                            </p>
                        </div>
                    </div>
                    
            </div>  
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label for="image">{{ trans('cruds.banner.fields.image') }}*</label>
                            <input type="file" id="image" name="image" class="form-control"  required>
                            @if($errors->has('image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.banner.fields.image_helper') }}
                            </p>
                        </div>  
                    </div>
            </div>
            <div>
                <input class="btn btn-primary float-right" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
