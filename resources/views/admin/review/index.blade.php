@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Review List
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            Id
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Comments
                        </th>
                         <th>
                            Status
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($reviews as $key => $review)
                        <tr data-entry-id="{{ $review->id }}">
                            <td>

                            </td>
                            <td>
                                {{ ++$key }}
                            </td>
                            <td>
                                {{ $review->name ?? '' }}
                            </td>
                            <td>
                                {{ $review->email ?? '' }}
                            </td>
                            <td >
                                {{ $review->comments ?? '' }}
                            </td >
                            @if($review->status=='1')
                            <td style="color:green;">
                               Approved
                            </td>
                            @else
                             <td style="color:orange;">
                              Pending
                            </td>
                            @endif
                            <td>
                                <a class="btn btn-sm btn-success" onclick="review_approve({{$review->id}})">
                                    Approve
                                </a>
                                <a class="btn btn-sm btn-primary" href="{{ route('admin.review.show', $review->id) }}">
                                    {{ trans('global.view') }}
                                </a>

                                <form action="{{ route('admin.review.destroy', $review->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-sm btn-danger" value="{{ trans('global.delete') }}">
                                </form>

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('users_manage')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.review.mass_destroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids}})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    //order: [[ 1, 'desc' ]],
    pageLength: 10,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTble()
            .columns.adjust();
    });
})

function review_approve(id){
  $.ajax({
        headers: {'x-csrf-token': _token},
        type: "POST",
        url: "review/approve",
        data: "id=" + id,
        success: function(data){
            alert(data.success);
            setTimeout(function(){
         window.location.reload();
}, 2000);
        }
    });
    }

</script>
@endsection