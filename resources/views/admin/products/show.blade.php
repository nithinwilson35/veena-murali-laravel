@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.product.title_singular') }}
        
       <a style="margin-top:5px; margin-right:10px; margin-left:10px;" class="btn btn-info float-right" href="{{ route('admin.products.edit', $product->id) }}">
                                   {{ trans('global.edit') }}
                                </a>
                                <a style="margin-top:5px;" class="btn btn-info float-right" href="{{ url('/admin/products') }}">
                {{ trans('global.back_to_list') }}
            </a>
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.id') }}
                        </th>
                        <td>
                            {{ $product->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.title') }}
                        </th>
                        <td>
                            {{ $product->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Product Code
                        </th>
                        <td>
                            {{ $product->sub_title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.category') }}
                        </th>
                        <td>
                            {{ $product->category->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.description') }}
                        </th>
                        <td>
                            {!! $product->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.height') }}
                        </th>
                        <td>
                            {{ $product->height }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.width') }}
                        </th>
                        <td>
                            {{ $product->width }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.length') }}
                        </th>
                        <td>
                            {{ $product->length }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.weight') }}
                        </th>
                        <td>
                            {{ $product->weight }}
                        </td>
                    </tr>
                    @if($product->chain_length != '')
                        <tr>
                            <th>
                                {{ trans('cruds.product.fields.chain_length') }}
                            </th>
                            <td>
                                {{ $product->chain_length }}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <th>
                            {{ trans('cruds.product.fields.status') }}
                        </th>
                        <td>
                                @if($product->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-danger">Inactive</span>
                                @endif
   
                        </td>
                    </tr>
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.stock_available') }}
                        </th>
                        <td>
                                @if($product->status == 1)
                                <span class="badge badge-success">Yes</span>
                                @else
                                <span class="badge badge-danger">No</span>
                                @endif
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.delivery_days') }}
                        </th>
                        <td>
                            {{ $product->delivery_days }}
                        </td>
                    </tr>
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.list_image') }}
                        </th>
                        <td>
                            <img class="img-height-view img-fluid img-thumbnail" src="{{url('storage/app/'.$product->list_image)}}" />
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.images') }}
                        </th>
                        <td>
                            @foreach($images as $image)    
                                <img class="img-height-view img-fluid img-thumbnail" src="{{url('storage/app/'.$image->image)}}" />
                            @endforeach
                        </td>
                </tr>
                <!--<tr>-->
                <!--        <th>-->
                <!--            Videos-->
                <!--        </th>-->
                <!--        <td>-->
                <!--            @foreach($videos as $video)-->
                <!--            <a href="#!" data-toggle="modal" data-target=".videopopup" >-->
                <!--                <img data-src="{{url('storage/app/'.$video->video)}}" style="max-height:150px;" class="img-height-view img-fluid img-thumbnail" src="{{url('/themes/play-button.png')}}" />-->
                <!--            </a>-->
                <!--            @endforeach-->
                <!--        </td>-->
                <!--</tr>-->
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.meta_title') }}
                        </th>
                        <td>
                            {{ $product->meta_title }}
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.meta_keywords') }}
                        </th>
                        <td>
                            {{ $product->meta_keywords }}
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.product.fields.meta_description') }}
                        </th>
                        <td>
                            {{ $product->meta_description }}
                        </td>
                </tr>
                </tbody>
            </table>
            
        </div>


    </div>
</div>
<div class="modal fade videopopup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <button type="button" class="close videobtn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="modal-body">
            <video id="recap" width="100%" controls controlslist="nodownload" autoplay="">
            <source src="" type="video/mp4">
            <!-- <source src="" type="video/ogg"> -->
            Your browser does not support the video tag.
            </video>
        </div>
    </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var imgposter = button.find("img").attr("src");
        var vidsrc = button.find("img").attr("data-src");

        //$(this).find('video#recap').attr('poster', imgposter);
        $(this).find('video#recap source').attr('src', '');
        $(this).find('video#recap source').attr('src', vidsrc);

        $(this).find('video#recap')[0].load();
    }).on('hidden.bs.modal', function(){
        $(this).find('video')[0].pause();
    });
    

</script>
@endsection