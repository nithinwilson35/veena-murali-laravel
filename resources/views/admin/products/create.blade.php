@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.product.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.products.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.product.fields.title') }}*</label>
                            <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($product) ? $product->title : '') }}" required>
                            @if($errors->has('title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.title_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('sub_title') ? 'has-error' : '' }}">
                            <label for="sub_title">Product Code</label>
                            <input type="text" id="sub_title" readonly name="sub_title" class="form-control" value="{{ old('sub_title', isset($product) ? $product->sub_title : '') }}" required>
                            @if($errors->has('sub_title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('sub_title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                System will auto generate the product code!
                            </p>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
                            <label for="category">{{ trans('cruds.product.fields.category') }}*</label>
                            <select name="category_id" id="category_id" class="form-control select2" required>
                                    <option value="">select</option>
                                    @foreach($parents as $parent)
                                        <option value="{{$parent->id}}">{{$parent->title}}</option>
                                    @endforeach
                            </select>
                            @if($errors->has('category_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('category_id') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.category_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('subcategory_id') ? 'has-error' : '' }}">
                            <label for="sub_category">{{ trans('cruds.product.fields.sub_category') }}*</label>
                            <select name="subcategory_id" id="subcategory_id" class="form-control select2" >
                                <option value="0">Select</option>
                                @foreach($childrens as $children)
                                        <option data-category="{{$children->parent_id}}" value="{{$children->id}}">{{$children->title}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('subcategory_id'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('subcategory_id') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.sub_category_helper') }}
                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('height') ? 'has-error' : '' }}">
                            <label for="height">{{ trans('cruds.product.fields.height') }}*</label>
                            <input type="text" id="height" name="height" class="form-control" value="{{ old('height', isset($product) ? $product->height : '') }}" required>
                            @if($errors->has('height'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('height') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.height_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('width') ? 'has-error' : '' }}">
                            <label for="width">{{ trans('cruds.product.fields.width') }}*</label>
                            <input type="text" id="width" name="width" class="form-control" value="{{ old('width', isset($product) ? $product->width : '') }}" required>
                            @if($errors->has('width'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('width') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.width_helper') }}
                            </p>
                        </div>
                    </div>
            </div>
            <div class="row">
                  <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('weight') ? 'has-error' : '' }}">
                            <label for="weight">{{ trans('cruds.product.fields.weight') }}*</label>
                            <input type="text" id="weight" name="weight" class="form-control" value="{{ old('weight', isset($product) ? $product->weight : '') }}" required>
                            @if($errors->has('weight'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('weight') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.weight_helper') }}
                            </p>
                        </div>
                    </div> 
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('length') ? 'has-error' : '' }}">
                            <label for="length">{{ trans('cruds.product.fields.length') }}*</label>
                            <input type="text" id="length" name="length" class="form-control" value="{{ old('length', isset($product) ? $product->length : '') }}" required>
                            @if($errors->has('length'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('length') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.length_helper') }}
                            </p>
                        </div>
                    </div> 
                    
            </div>
            <div class="row">
                <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('chain_length') ? 'has-error' : '' }}">
                            <label for="chain_length">{{ trans('cruds.product.fields.chain_length') }}</label>
                            <input type="text" id="chain_length" name="chain_length" class="form-control" value="{{ old('chain_length', isset($product) ? $product->chain_length : '') }}">
                            @if($errors->has('chain_length'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('chain_length') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                 {{ trans('cruds.product.fields.chain_length_helper') }}
                            </p>
                        </div>
                    </div> 
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            <label for="status">{{ trans('cruds.product.fields.status') }}*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            @if($errors->has('status'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.status_helper') }}
                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                   
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('stock_available') ? 'has-error' : '' }}">
                            <label for="stock_available">{{ trans('cruds.product.fields.stock_available') }}*</label>
                            <select name="stock_available" id="stock_available" class="form-control select2" required>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                            @if($errors->has('stock_available'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('stock_available') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.stock_available_helper') }}
                            </p>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('delivery_days') ? 'has-error' : '' }}">
                            <label for="delivery_days">{{ trans('cruds.product.fields.delivery_days') }}*</label>
                            <input type="text" id="delivery_days" name="delivery_days" class="form-control" value="{{ old('delivery_days', isset($product) ? $product->delivery_days : '') }}" required>
                            @if($errors->has('delivery_days'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('delivery_days') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.delivery_days_helper') }}
                            </p>
                        </div>
                    </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                        <label for="description">{{ trans('cruds.product.fields.description') }}</label>
                        <textarea id="description" name="description" class="form-control ckeditor">{{ old('description', isset($product) ? $product->description : '') }}</textarea>
                        @if($errors->has('description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.product.fields.description_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('list_image') ? 'has-error' : '' }}">
                            <label for="list_image">{{ trans('cruds.product.fields.list_image') }}*</label>
                            <input type="file" id="list_image" name="list_image" class="form-control"  required>
                            @if($errors->has('list_image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('list_image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.list_image_helper') }}
                            </p>
                        </div>    
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('images') ? 'has-error' : '' }}">
                            <label for="images">{{ trans('cruds.product.fields.images') }}*</label>
                            <input type="file" id="images" name="images[]" multiple class="form-control"  required>
                            @if($errors->has('images'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('images') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.images_helper') }}
                            </p>
                        </div>    
                    </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('videos') ? 'has-error' : '' }}">
                        <label for="videos">Videos (Youtube IDs)</label>
                        <input type="text" id="videos" name="videos" class="form-control" >
                        @if($errors->has('videos'))
                            <em class="invalid-feedback">
                                {{ $errors->first('videos') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            
                        </p>
                    </div>    
                </div>
            </div>
            <h5 class="mb-20">SEO Content</h5>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                            <label for="meta_title">{{ trans('cruds.product.fields.meta_title') }}</label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control"  >
                            @if($errors->has('meta_title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('meta_title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.product.fields.meta_title_helper') }}
                            </p>
                        </div>    
                    </div>
                    
            </div>
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                        <label for="meta_keywords">{{ trans('cruds.product.fields.meta_keywords') }}</label>
                        <textarea id="bottom_content" name="meta_keywords" class="form-control" >{{ old('meta_keywords', isset($product) ? $product->meta_keywords : '') }}</textarea>
                        @if($errors->has('meta_keywords'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_keywords') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.product.fields.meta_keywords_helper') }}
                        </p>
                    </div>
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                        <label for="meta_description">{{ trans('cruds.product.fields.meta_description') }}</label>
                        <textarea id="meta_description" name="meta_description" class="form-control" >{{ old('meta_description', isset($product) ? $product->meta_description : '') }}</textarea>
                        @if($errors->has('meta_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.product.fields.meta_description_helper') }}
                        </p>
                    </div>
                    </div>
            </div>
                    
            <div>
                <input class="btn btn-primary float-right" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
$("#category_id").on('change', function(e){
        e.preventDefault();
        var val = $(this).val();
        $("#subcategory_id").val('');
        $("#subcategory_id option").attr('disabled', true);
        $("#subcategory_id option[data-category=" + val + "]").attr('disabled', false);
        $("#subcategory_id").select2('destroy').select2();
    });
</script>
@endsection