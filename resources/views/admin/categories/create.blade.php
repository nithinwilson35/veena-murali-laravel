@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.category.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.categories.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.category.fields.title') }}*</label>
                            <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($category) ? $category->title : '') }}" required>
                            @if($errors->has('title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.title_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('heading') ? 'has-error' : '' }}">
                            <label for="heading">{{ trans('cruds.category.fields.heading') }}*</label>
                            <input type="text" id="heading" name="heading" class="form-control" value="{{ old('heading', isset($category) ? $category->heading : '') }}" required>
                            @if($errors->has('heading'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('heading') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.heading_helper') }}
                            </p>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                            <label for="parent">{{ trans('cruds.category.fields.parent') }}</label>
                            <select name="parent_id" id="parent" class="form-control select2">
                                    <option value="0">select</option>
                                    @foreach($parents as $parent)
                                        <option value="{{$parent->id}}">{{$parent->title}}</option>
                                    @endforeach
                            </select>
                            @if($errors->has('parent'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('parent') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.parent_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}">
                            <label for="status">{{ trans('cruds.category.fields.status') }}*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            @if($errors->has('status'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.status_helper') }}
                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                        <label for="description">{{ trans('cruds.category.fields.description') }}</label>
                        <textarea id="description" name="description" class="form-control ckeditor">{{ old('description', isset($category) ? $category->description : '') }}</textarea>
                        @if($errors->has('description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.category.fields.description_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('banner_image') ? 'has-error' : '' }}">
                            <label for="banner_image">{{ trans('cruds.category.fields.banner_image') }}*</label>
                            <input type="file" id="banner_image" name="banner_image" class="form-control"  required>
                            @if($errors->has('banner_image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('banner_image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.banner_image_helper') }}
                            </p>
                        </div>    
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('bottom_content') ? 'has-error' : '' }}">
                        <label for="description">{{ trans('cruds.category.fields.bottom_content') }}</label>
                        <textarea id="bottom_content" name="bottom_content" class="form-control" required>{{ old('bottom_content', isset($category) ? $category->bottom_content : '') }}</textarea>
                        @if($errors->has('bottom_content'))
                            <em class="invalid-feedback">
                                {{ $errors->first('bottom_content') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.category.fields.bottom_content_helper') }}
                        </p>
                    </div>
                    </div>
            </div>
            <h5 class="mb-20">SEO Content</h5>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                            <label for="meta_title">{{ trans('cruds.category.fields.meta_title') }}</label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control"  >
                            @if($errors->has('meta_title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('meta_title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.category.fields.meta_title_helper') }}
                            </p>
                        </div>    
                    </div>
                    
            </div>
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                        <label for="meta_keywords">{{ trans('cruds.category.fields.meta_keywords') }}</label>
                        <textarea id="bottom_content" name="meta_keywords" class="form-control" >{{ old('meta_keywords', isset($category) ? $category->meta_keywords : '') }}</textarea>
                        @if($errors->has('meta_keywords'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_keywords') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.category.fields.meta_keywords_helper') }}
                        </p>
                    </div>
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                        <label for="meta_description">{{ trans('cruds.category.fields.meta_description') }}</label>
                        <textarea id="meta_description" name="meta_description" class="form-control" >{{ old('meta_description', isset($category) ? $category->meta_description : '') }}</textarea>
                        @if($errors->has('meta_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.category.fields.meta_description_helper') }}
                        </p>
                    </div>
                    </div>
            </div>
                    
            <div>
                <input class="btn btn-primary float-right" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
