@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.category.title_singular') }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.category.fields.id') }}
                        </th>
                        <td>
                            {{ $category->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.category.fields.title') }}
                        </th>
                        <td>
                            {{ $category->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.category.fields.heading') }}
                        </th>
                        <td>
                            {{ $category->heading }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.category.fields.description') }}
                        </th>
                        <td>
                            {!! $category->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.category.fields.parent') }}
                        </th>
                        <td>
                            {{ $category->heading }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.category.fields.status') }}
                        </th>
                        <td>
                                @if($category->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-danger">Inactive</span>
                                @endif
   
                        </td>
                    </tr>
                <tr>
                        <th>
                            {{ trans('cruds.category.fields.bottom_content') }}
                        </th>
                        <td>
                            {{ $category->bottom_content }}
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.category.fields.banner_image') }}
                        </th>
                        <td>
                            <img class="img-fluid img-thumbnail" src="{{url('storage/app/'.$category->banner_image)}}" />
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.category.fields.meta_title') }}
                        </th>
                        <td>
                            {{ $category->meta_title }}
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.category.fields.meta_keywords') }}
                        </th>
                        <td>
                            {{ $category->meta_keywords }}
                        </td>
                </tr>
                <tr>
                        <th>
                            {{ trans('cruds.category.fields.meta_description') }}
                        </th>
                        <td>
                            {{ $category->meta_description }}
                        </td>
                </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection