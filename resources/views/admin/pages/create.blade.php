@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.page.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.pages.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label for="name">{{ trans('cruds.page.fields.title') }}*</label>
                            <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($page) ? $page->title : '') }}" required>
                            @if($errors->has('title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.page.fields.title_helper') }}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('heading') ? 'has-error' : '' }}">
                            <label for="heading">{{ trans('cruds.page.fields.heading') }}*</label>
                            <input type="text" id="heading" name="heading" class="form-control" value="{{ old('heading', isset($page) ? $page->heading : '') }}" required>
                            @if($errors->has('heading'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('heading') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.page.fields.heading_helper') }}
                            </p>
                        </div>
                    </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
                        <label for="content">{{ trans('cruds.page.fields.content') }}</label>
                        <textarea id="content" name="content" class="form-control ckeditor">{{ old('content', isset($page) ? $page->content : '') }}</textarea>
                        @if($errors->has('content'))
                            <em class="invalid-feedback">
                                {{ $errors->first('content') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.page.fields.content_helper') }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label for="image">{{ trans('cruds.page.fields.image') }}*</label>
                            <input type="file" id="image" name="image" class="form-control" >
                            @if($errors->has('image'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('image') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.page.fields.image_helper') }}
                            </p>
                        </div>    
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                            <label for="status">{{ trans('cruds.page.fields.status') }}*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            @if($errors->has('status'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.page.fields.status_helper') }}
                            </p>
                        </div>  
                    </div>
            </div>
            <h5 class="mb-20">SEO Content</h5>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                            <label for="meta_title">{{ trans('cruds.page.fields.meta_title') }}</label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control"  >
                            @if($errors->has('meta_title'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('meta_title') }}
                                </em>
                            @endif
                            <p class="helper-block">
                                {{ trans('cruds.page.fields.meta_title_helper') }}
                            </p>
                        </div>    
                    </div>
                    
            </div>
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('meta_keywords') ? 'has-error' : '' }}">
                        <label for="meta_keywords">{{ trans('cruds.page.fields.meta_keywords') }}</label>
                        <textarea id="bottom_content" name="meta_keywords" class="form-control" >{{ old('meta_keywords', isset($page) ? $page->meta_keywords : '') }}</textarea>
                        @if($errors->has('meta_keywords'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_keywords') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.page.fields.meta_keywords_helper') }}
                        </p>
                    </div>
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                        <label for="meta_description">{{ trans('cruds.page.fields.meta_description') }}</label>
                        <textarea id="meta_description" name="meta_description" class="form-control" >{{ old('meta_description', isset($page) ? $page->meta_description : '') }}</textarea>
                        @if($errors->has('meta_description'))
                            <em class="invalid-feedback">
                                {{ $errors->first('meta_description') }}
                            </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.page.fields.meta_description_helper') }}
                        </p>
                    </div>
                    </div>
            </div>
                    
            <div>
                <input class="btn btn-primary float-right" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection
