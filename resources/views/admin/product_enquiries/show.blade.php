@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.product_enquiry.title_singular') }}
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.product_enquiry.fields.id') }}
                        </th>
                        <td>
                            {{ $product_enquiry->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product_enquiry.fields.product') }}
                        </th>
                        <td>
                            {{ $product_enquiry->product->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product_enquiry.fields.name') }}
                        </th>
                        <td>
                            {{ $product_enquiry->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product_enquiry.fields.email') }}
                        </th>
                        <td>
                            {{ $product_enquiry->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product_enquiry.fields.phone') }}
                        </th>
                        <td>
                            {{ $product_enquiry->phone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.product_enquiry.fields.message') }}
                        </th>
                        <td>
                               {{ $product_enquiry->message }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="{{ url()->previous() }}">
                {{ trans('global.back_to_list') }}
            </a>
        </div>


    </div>
</div>
@endsection