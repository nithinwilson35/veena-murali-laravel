<!doctype html>
<html lang="en">
  <head>
    <title>{{ $seo->meta_title ?? 'Veena Murali Decors' }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="og:title" property="og:title" content="{{ $seo->meta_title ?? 'Veena Murali Decors' }}">
    @if(isset($seo->list_image))
        <meta property="og:image" content="{{url('storage/app/'.$seo->list_image)}}">
        <meta property="og:image:width" content="600" />
<meta property="og:image:height" content="600" />
@else
<meta property="og:image" content="{{asset('themes/frontend/assets/images/logo.v1.png')}}">
        <meta property="og:image:width" content="600" />
<meta property="og:image:height" content="600" />
    @endif
    <meta name="description" content="{{ $seo->meta_description ?? 'Veena Murali Decors' }}">
    <meta name="keywords" content="{{ $seo->meta_keywords ?? 'Veena Murali Decors' }}" />
    <link rel="stylesheet" href="{{asset('themes/frontend/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('themes/frontend/assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('themes/frontend/assets/css/aos.css')}}">
    <link rel="stylesheet" href="{{asset('themes/frontend/assets/css/style.css')}}">
    <link rel="shortcut icon"href="{{asset('themes/frontend/assets/images/vm-icon.ico')}}"/>
    <meta name="theme-color" content="#21304A" />
    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K9ZP5DD');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body style="background:#fffaf2;">
      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9ZP5DD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  @include('includes.header')
    <!--header-->
    @yield('content')

    @include('includes.footer')
    <!--footer-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{asset('themes/frontend/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/slick.min.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/svg-script.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/aos.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/afterglow.min.js')}}"></script>
    <script src="{{asset('themes/frontend/assets/js/script.js')}}"></script>
    <script>
      AOS.init();
    </script>
    @yield('scripts')
 </body>
</html>


