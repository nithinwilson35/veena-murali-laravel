<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        Review List
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            Id
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Comments
                        </th>
                         <th>
                            Status
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $reviews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($review->id); ?>">
                            <td>

                            </td>
                            <td>
                                <?php echo e(++$key); ?>

                            </td>
                            <td>
                                <?php echo e($review->name ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($review->email ?? ''); ?>

                            </td>
                            <td >
                                <?php echo e($review->comments ?? ''); ?>

                            </td >
                            <?php if($review->status=='1'): ?>
                            <td style="color:green;">
                               Approved
                            </td>
                            <?php else: ?>
                             <td style="color:orange;">
                              Pending
                            </td>
                            <?php endif; ?>
                            <td>
                                <a class="btn btn-sm btn-success" onclick="review_approve(<?php echo e($review->id); ?>)">
                                    Approve
                                </a>
                                <a class="btn btn-sm btn-primary" href="<?php echo e(route('admin.review.show', $review->id)); ?>">
                                    <?php echo e(trans('global.view')); ?>

                                </a>

                                <form action="<?php echo e(route('admin.review.destroy', $review->id)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <input type="submit" class="btn btn-sm btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                </form>

                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('users_manage')): ?>
  let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "<?php echo e(route('admin.review.mass_destroy')); ?>",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

        return
      }

      if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids}})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
<?php endif; ?>

  $.extend(true, $.fn.dataTable.defaults, {
    //order: [[ 1, 'desc' ]],
    pageLength: 10,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTble()
            .columns.adjust();
    });
})

function review_approve(id){
  $.ajax({
        headers: {'x-csrf-token': _token},
        type: "POST",
        url: "review/approve",
        data: "id=" + id,
        success: function(data){
            alert(data.success);
            setTimeout(function(){
         window.location.reload();
}, 2000);
        }
    });
    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/review/index.blade.php ENDPATH**/ ?>