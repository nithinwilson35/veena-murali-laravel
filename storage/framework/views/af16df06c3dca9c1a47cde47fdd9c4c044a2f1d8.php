<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        View Review
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            Id
                        </th>
                        <td>
                            <?php echo e($review->id); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                           Name
                        </th>
                        <td>
                            <?php echo e($review->name); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                            <?php echo e($review->email); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                            <?php echo e($review->title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            Comments
                        </th>
                        <td>
                               <?php echo e($review->comments); ?>

                        </td>
                    </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="<?php echo e(url()->previous()); ?>">
                <?php echo e(trans('global.back_to_list')); ?>

            </a>
        </div>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/review/show.blade.php ENDPATH**/ ?>