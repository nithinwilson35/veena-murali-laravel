<table style="width:90%;margin:auto;display:block;margin-top:5%;background-color:#fff;border-left:20px solid #21304A;">
          <tbody style="width:80%;display:block;padding:5%">
              <tr style="width:100%">
                  <td><img style="width:150px;" src="<?php echo e(asset('/themes/frontend/assets/images/logo.png')); ?>" class="CToWUd"></td>
              </tr>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px"><br><br><b>Hi ,<br><br></b></td>
              </tr>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px;line-height:1.9">An enquiry has been
received<br><br></td>
              </tr>
              <?php if(isset($content['product'])): ?>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px">Product&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo e($content['product']); ?><br><br></td>
              </tr>
              <?php endif; ?>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo e($content['name']); ?><br><br></td>
              </tr>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px">Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo e($content['email']); ?><br><br></td>
              </tr>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px">Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo e($content['phone']); ?><br><br></td>
              </tr>
              <tr style="width:100%;display:block">
                  <td style="padding-left:3%;width:100%;display:block;font-size:20px">Message&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo e($content['message']); ?><br><br></td>
              </tr>
          </tbody>
          
      </table><?php /**PATH /home2/veenamur/public_html/resources/views/emails/contact.blade.php ENDPATH**/ ?>