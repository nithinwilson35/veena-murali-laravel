<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.show')); ?> <?php echo e(trans('cruds.banner.title_singular')); ?>

    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.banner.fields.id')); ?>

                        </th>
                        <td>
                            <?php echo e($banner->id); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.banner.fields.title')); ?>

                        </th>
                        <td>
                            <?php echo e($banner->title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.banner.fields.link_text')); ?>

                        </th>
                        <td>
                            <?php echo e($banner->link_text); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.banner.fields.link_url')); ?>

                        </th>
                        <td>
                            <?php echo e($banner->link_url); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.banner.fields.status')); ?>

                        </th>
                        <td>
                                <?php if($banner->status == 1): ?>
                                <span class="badge badge-success">Active</span>
                                <?php else: ?>
                                <span class="badge badge-danger">Inactive</span>
                                <?php endif; ?>
   
                        </td>
                    </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.banner.fields.image')); ?>

                        </th>
                        <td>
                            <img class="img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$banner->image)); ?>" />
                        </td>
                </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="<?php echo e(url()->previous()); ?>">
                <?php echo e(trans('global.back_to_list')); ?>

            </a>
        </div>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/banners/show.blade.php ENDPATH**/ ?>