<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('cruds.product.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.products.store")); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                            <label for="name"><?php echo e(trans('cruds.product.fields.title')); ?>*</label>
                            <input type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($product) ? $product->title : '')); ?>" required>
                            <?php if($errors->has('title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.title_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('sub_title') ? 'has-error' : ''); ?>">
                            <label for="sub_title">Product Code</label>
                            <input type="text" id="sub_title" readonly name="sub_title" class="form-control" value="<?php echo e(old('sub_title', isset($product) ? $product->sub_title : '')); ?>" required>
                            <?php if($errors->has('sub_title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('sub_title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                System will auto generate the product code!
                            </p>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('category_id') ? 'has-error' : ''); ?>">
                            <label for="category"><?php echo e(trans('cruds.product.fields.category')); ?>*</label>
                            <select name="category_id" id="category_id" class="form-control select2" required>
                                    <option value="">select</option>
                                    <?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($parent->id); ?>"><?php echo e($parent->title); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('category_id')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('category_id')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.category_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('subcategory_id') ? 'has-error' : ''); ?>">
                            <label for="sub_category"><?php echo e(trans('cruds.product.fields.sub_category')); ?>*</label>
                            <select name="subcategory_id" id="subcategory_id" class="form-control select2" >
                                <option value="0">Select</option>
                                <?php $__currentLoopData = $childrens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $children): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option data-category="<?php echo e($children->parent_id); ?>" value="<?php echo e($children->id); ?>"><?php echo e($children->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('subcategory_id')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('subcategory_id')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.sub_category_helper')); ?>

                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('height') ? 'has-error' : ''); ?>">
                            <label for="height"><?php echo e(trans('cruds.product.fields.height')); ?>*</label>
                            <input type="text" id="height" name="height" class="form-control" value="<?php echo e(old('height', isset($product) ? $product->height : '')); ?>" required>
                            <?php if($errors->has('height')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('height')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.height_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('width') ? 'has-error' : ''); ?>">
                            <label for="width"><?php echo e(trans('cruds.product.fields.width')); ?>*</label>
                            <input type="text" id="width" name="width" class="form-control" value="<?php echo e(old('width', isset($product) ? $product->width : '')); ?>" required>
                            <?php if($errors->has('width')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('width')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.width_helper')); ?>

                            </p>
                        </div>
                    </div>
            </div>
            <div class="row">
                  <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('weight') ? 'has-error' : ''); ?>">
                            <label for="weight"><?php echo e(trans('cruds.product.fields.weight')); ?>*</label>
                            <input type="text" id="weight" name="weight" class="form-control" value="<?php echo e(old('weight', isset($product) ? $product->weight : '')); ?>" required>
                            <?php if($errors->has('weight')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('weight')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.weight_helper')); ?>

                            </p>
                        </div>
                    </div> 
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('length') ? 'has-error' : ''); ?>">
                            <label for="length"><?php echo e(trans('cruds.product.fields.length')); ?>*</label>
                            <input type="text" id="length" name="length" class="form-control" value="<?php echo e(old('length', isset($product) ? $product->length : '')); ?>" required>
                            <?php if($errors->has('length')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('length')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.length_helper')); ?>

                            </p>
                        </div>
                    </div> 
                    
            </div>
            <div class="row">
                <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('chain_length') ? 'has-error' : ''); ?>">
                            <label for="chain_length"><?php echo e(trans('cruds.product.fields.chain_length')); ?></label>
                            <input type="text" id="chain_length" name="chain_length" class="form-control" value="<?php echo e(old('chain_length', isset($product) ? $product->chain_length : '')); ?>">
                            <?php if($errors->has('chain_length')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('chain_length')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                 <?php echo e(trans('cruds.product.fields.chain_length_helper')); ?>

                            </p>
                        </div>
                    </div> 
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
                            <label for="status"><?php echo e(trans('cruds.product.fields.status')); ?>*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            <?php if($errors->has('status')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('status')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.status_helper')); ?>

                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                   
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('stock_available') ? 'has-error' : ''); ?>">
                            <label for="stock_available"><?php echo e(trans('cruds.product.fields.stock_available')); ?>*</label>
                            <select name="stock_available" id="stock_available" class="form-control select2" required>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                            <?php if($errors->has('stock_available')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('stock_available')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.stock_available_helper')); ?>

                            </p>
                        </div>  
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('delivery_days') ? 'has-error' : ''); ?>">
                            <label for="delivery_days"><?php echo e(trans('cruds.product.fields.delivery_days')); ?>*</label>
                            <input type="text" id="delivery_days" name="delivery_days" class="form-control" value="<?php echo e(old('delivery_days', isset($product) ? $product->delivery_days : '')); ?>" required>
                            <?php if($errors->has('delivery_days')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('delivery_days')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.delivery_days_helper')); ?>

                            </p>
                        </div>
                    </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group <?php echo e($errors->has('description') ? 'has-error' : ''); ?>">
                        <label for="description"><?php echo e(trans('cruds.product.fields.description')); ?></label>
                        <textarea id="description" name="description" class="form-control ckeditor"><?php echo e(old('description', isset($product) ? $product->description : '')); ?></textarea>
                        <?php if($errors->has('description')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('description')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.product.fields.description_helper')); ?>

                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('list_image') ? 'has-error' : ''); ?>">
                            <label for="list_image"><?php echo e(trans('cruds.product.fields.list_image')); ?>*</label>
                            <input type="file" id="list_image" name="list_image" class="form-control"  required>
                            <?php if($errors->has('list_image')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('list_image')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.list_image_helper')); ?>

                            </p>
                        </div>    
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('images') ? 'has-error' : ''); ?>">
                            <label for="images"><?php echo e(trans('cruds.product.fields.images')); ?>*</label>
                            <input type="file" id="images" name="images[]" multiple class="form-control"  required>
                            <?php if($errors->has('images')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('images')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.images_helper')); ?>

                            </p>
                        </div>    
                    </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group <?php echo e($errors->has('videos') ? 'has-error' : ''); ?>">
                        <label for="videos">Videos (Youtube IDs)</label>
                        <input type="text" id="videos" name="videos" class="form-control" >
                        <?php if($errors->has('videos')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('videos')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            
                        </p>
                    </div>    
                </div>
            </div>
            <h5 class="mb-20">SEO Content</h5>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('meta_title') ? 'has-error' : ''); ?>">
                            <label for="meta_title"><?php echo e(trans('cruds.product.fields.meta_title')); ?></label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control"  >
                            <?php if($errors->has('meta_title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('meta_title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.product.fields.meta_title_helper')); ?>

                            </p>
                        </div>    
                    </div>
                    
            </div>
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('meta_keywords') ? 'has-error' : ''); ?>">
                        <label for="meta_keywords"><?php echo e(trans('cruds.product.fields.meta_keywords')); ?></label>
                        <textarea id="bottom_content" name="meta_keywords" class="form-control" ><?php echo e(old('meta_keywords', isset($product) ? $product->meta_keywords : '')); ?></textarea>
                        <?php if($errors->has('meta_keywords')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('meta_keywords')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.product.fields.meta_keywords_helper')); ?>

                        </p>
                    </div>
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('meta_description') ? 'has-error' : ''); ?>">
                        <label for="meta_description"><?php echo e(trans('cruds.product.fields.meta_description')); ?></label>
                        <textarea id="meta_description" name="meta_description" class="form-control" ><?php echo e(old('meta_description', isset($product) ? $product->meta_description : '')); ?></textarea>
                        <?php if($errors->has('meta_description')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('meta_description')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.product.fields.meta_description_helper')); ?>

                        </p>
                    </div>
                    </div>
            </div>
                    
            <div>
                <input class="btn btn-primary float-right" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
$("#category_id").on('change', function(e){
        e.preventDefault();
        var val = $(this).val();
        $("#subcategory_id").val('');
        $("#subcategory_id option").attr('disabled', true);
        $("#subcategory_id option[data-category=" + val + "]").attr('disabled', false);
        $("#subcategory_id").select2('destroy').select2();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/products/create.blade.php ENDPATH**/ ?>