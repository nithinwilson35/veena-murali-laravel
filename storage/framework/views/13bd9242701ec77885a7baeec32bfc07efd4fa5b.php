<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.edit')); ?> <?php echo e(trans('cruds.category.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.categories.update", [$category->id])); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                            <label for="name"><?php echo e(trans('cruds.category.fields.title')); ?>*</label>
                            <input type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($category) ? $category->title : '')); ?>" required>
                            <?php if($errors->has('title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.category.fields.title_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('heading') ? 'has-error' : ''); ?>">
                            <label for="heading"><?php echo e(trans('cruds.category.fields.heading')); ?>*</label>
                            <input type="text" id="heading" name="heading" class="form-control" value="<?php echo e(old('heading', isset($category) ? $category->heading : '')); ?>" required>
                            <?php if($errors->has('heading')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('heading')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.category.fields.heading_helper')); ?>

                            </p>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('roles') ? 'has-error' : ''); ?>">
                            <label for="parent"><?php echo e(trans('cruds.category.fields.parent')); ?></label>
                            <select name="parent_id" id="parent" class="form-control select2">
                                    <option <?php if($category->parent_id == 0): ?> selected <?php endif; ?> value="0">select</option>
                                    <?php $__currentLoopData = $parents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $parent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option <?php if($category->parent_id == $parent->id): ?> selected <?php endif; ?> value="<?php echo e($parent->id); ?>"><?php echo e($parent->title); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('parent')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('parent')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.category.fields.parent_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('roles') ? 'has-error' : ''); ?>">
                            <label for="status"><?php echo e(trans('cruds.category.fields.status')); ?>*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            <?php if($errors->has('status')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('status')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.category.fields.status_helper')); ?>

                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group <?php echo e($errors->has('description') ? 'has-error' : ''); ?>">
                        <label for="description"><?php echo e(trans('cruds.category.fields.description')); ?></label>
                        <textarea id="description" name="description" class="form-control ckeditor" ><?php echo e(old('description', isset($category) ? $category->description : '')); ?></textarea>
                        <?php if($errors->has('description')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('description')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.category.fields.description_helper')); ?>

                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('banner_image') ? 'has-error' : ''); ?>">
                            <label for="banner_image"><?php echo e(trans('cruds.category.fields.banner_image')); ?></label>
                            <input type="file" id="banner_image" name="banner_image" class="form-control" >
                            <?php if($errors->has('banner_image')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('banner_image')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.category.fields.banner_image_helper')); ?>

                            </p>
                            <img class="img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$category->banner_image)); ?>" />
                        </div>    
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('bottom_content') ? 'has-error' : ''); ?>">
                        <label for="description"><?php echo e(trans('cruds.category.fields.bottom_content')); ?></label>
                        <textarea id="bottom_content" name="bottom_content" class="form-control" required><?php echo e(old('bottom_content', isset($category) ? $category->bottom_content : '')); ?></textarea>
                        <?php if($errors->has('bottom_content')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('bottom_content')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.category.fields.bottom_content_helper')); ?>

                        </p>
                    </div>
                    </div>
            </div>
            <h5 class="mb-20">SEO Content</h5>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('meta_title') ? 'has-error' : ''); ?>">
                            <label for="meta_title"><?php echo e(trans('cruds.category.fields.meta_title')); ?></label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control" value="<?php echo e(old('meta_title', isset($category) ? $category->meta_title : '')); ?>" >
                            <?php if($errors->has('meta_title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('meta_title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.category.fields.meta_title_helper')); ?>

                            </p>
                        </div>    
                    </div>
                    
            </div>
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('meta_keywords') ? 'has-error' : ''); ?>">
                        <label for="meta_keywords"><?php echo e(trans('cruds.category.fields.meta_keywords')); ?></label>
                        <textarea id="bottom_content" name="meta_keywords" class="form-control" ><?php echo e(old('meta_keywords', isset($category) ? $category->meta_keywords : '')); ?></textarea>
                        <?php if($errors->has('meta_keywords')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('meta_keywords')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.category.fields.meta_keywords_helper')); ?>

                        </p>
                    </div>
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('meta_description') ? 'has-error' : ''); ?>">
                        <label for="meta_description"><?php echo e(trans('cruds.category.fields.meta_description')); ?></label>
                        <textarea id="meta_description" name="meta_description" class="form-control" ><?php echo e(old('meta_description', isset($category) ? $category->meta_description : '')); ?></textarea>
                        <?php if($errors->has('meta_description')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('meta_description')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.category.fields.meta_description_helper')); ?>

                        </p>
                    </div>
                    </div>
            </div>
            <div>
                <input class="btn btn-primary float-right" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/categories/edit.blade.php ENDPATH**/ ?>