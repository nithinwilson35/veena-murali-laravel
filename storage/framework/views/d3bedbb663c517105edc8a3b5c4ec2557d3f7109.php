<?php $__env->startSection('content'); ?>
<div class="content">
    <div class="row">
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-tasks bg-primary p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-primary"><?php echo e(\App\Models\Category::active()->count()); ?></div>
                    <div class="text-muted text-uppercase font-weight-bold small">Categories</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="<?php echo e(route('admin.categories.index')); ?>">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-box-open bg-success p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-success"><?php echo e(\App\Models\Product::active()->count()); ?></div>
                    <div class="text-muted text-uppercase font-weight-bold small">Products</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="<?php echo e(route('admin.products.index')); ?>">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-images bg-warning p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-warning"><?php echo e(\App\Models\Banner::active()->count()); ?></div>
                    <div class="text-muted text-uppercase font-weight-bold small">Banners</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="<?php echo e(route('admin.banners.index')); ?>">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-6 col-lg-3">
            <div class="card">
                <div class="card-body p-6 d-flex align-items-center">
                    <i class="fa fa-book-open bg-danger p-3 font-2xl mr-3"></i>
                <div>
                <div class="text-value-sm text-danger"><?php echo e(\App\Models\Page::count()); ?></div>
                    <div class="text-muted text-uppercase font-weight-bold small">Pages</div>
                    </div>
                </div>
                <div class="card-footer px-3 py-6">
                    <a class="btn-block text-muted d-flex justify-content-between align-items-center" href="<?php echo e(route('admin.pages.index')); ?>">
                        <span class="small font-weight-bold">View All</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-group mb-4">
        <div class="card">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="text-success fas fa-envelope"></i>
                </div>
                <div class="text-value text-success"><?php echo e(\App\Models\Enquiry::count()); ?></div>
                <small class="text-muted clr-def text-uppercase font-weight-bold">Enquiries</small>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="h1 text-muted text-right mb-4">
                    <i class="text-primary fas fa-envelope-square"></i>
                </div>
                <div class="text-value text-primary"><?php echo e(\App\Models\ProductEnquiry::count()); ?></div>
                <small class="text-muted clr-def text-uppercase font-weight-bold">Product Enquiries</small>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/home.blade.php ENDPATH**/ ?>