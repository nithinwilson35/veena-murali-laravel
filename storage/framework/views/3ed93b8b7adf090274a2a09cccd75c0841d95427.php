<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.loader {
  border: 8px solid #f3f3f3;
  border-radius: 50%;
  border-top: 8px solid #3498db;
  width: 30px;
  height: 30px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes  spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.checked {
  color: #ffa516;
}
section.product-d-main .container-fluid.pd-box .right-area .slider-out-side .pd-slider .video-box .video-icon, .video-thmp .video-thumb-box {
    width:100%;
    height:100%;
    position: absolute;
    top:0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgb(0 0 0 / 48%);
    z-index: 9999;
    display: flex;
    justify-content: center;
    align-items: center;
}
section.product-d-main .container-fluid.pd-box .right-area .slider-out-side .pd-slider .video-box .video-icon img {
    width: 150px;
    height: 150px;
}
.video-thmp .video-thumb-box {
    z-index: 9 !important;
    cursor: pointer;
}
.video-thmp {
    position: relative;
}
.video-thmp .video-thumb-box img {
    width: 100%;
    border: 0 !important;
}
.video-box video {
    width: 100%;
}
.video-box .modal-dialog {
    width: 650px !important;
}
.pd-slider-nav1 .item img {
    border: 1px solid #cccccc;
}

h1 {
        font-family: "Playfair Display", serif;
    font-size: 4.375rem;
    font-weight: 400;
    line-height: 74px;
    color: #21304A;
    margin: 75px auto;
}

.category-link {
        width: 220px;
    margin: 0 auto;
    text-align: center;
    padding: 20px;
    border-radius: 50px;
    margin-bottom: 50px;
    background: #21304A;
    color: #ffffff;
}
@media (max-width: 762px) {
    .video-box .modal-dialog {
        width: 320px !important;
        margin: 0 auto;
        padding-top: 30px;
    }
    .video-box .modal-dialog {
        align-items: flex-start !important;
    }
}
</style>
<section class="product-d-main">
        <div class="container-fluid pd-box text-center">
        <h1>Product Unavailable</h1>
          
        </div>
        <div class="container-fluid pd-box text-center">
            <a href="<?php echo e(url('/'.$category->slug)); ?>" class="category-link" >See More in <?php echo e($category->title); ?>

                </a> 
        </div>
    </section>
   

   


<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script type="text/javascript">
    $('.modal').on('hide.bs.modal', function() {
     var memory = $(this).html();
     $(this).html(memory);
});
</script>
<script>


$(function () {
      
      $("#contact_submit").on('click', function(e){
        $("#msg_div").hide();
        $("#error_div").hide();
        e.preventDefault();
        if (!$("#contact-form")[0].checkValidity || $("#contact-form")[0].checkValidity()) {
          var data = $("#contact-form").serialize();
          $.ajax({
              type: "POST",
              url: $("#contact-form").attr('action'),
              data:  data,
              beforeSend: function() {
                $("#contact-form")[0].reset();
                $("#msg_div").show('slow');
              },
              success: function(response) {
                  if(response.status) {
                  }
              }
          });
        } else {
            $("#error_div").show('slow');
        }
      })
    });
    $('.success_msg').hide();
    $('.error_msg').hide();
     $('.loader').hide();
    $('#review_submit').click(function(e) {
    e.preventDefault();
    $('.error_msg').hide();
    $('.loader').show();
    var name= $('#name').val();
    var title= $('#title').val();
    var rating= $('#rating').val();
    var comments= $('#comments').val();
    if(name!=''&&title!=''&&rating!=''&&comments!=''){
	var form = $('#add_review')[0];
        var formData = new FormData(form);
    $.ajax({
      data: formData,
      url: 'review/store',
      type: "POST",
	  cache:false,
      contentType: false,
      processData: false,
      success: function(data) {
        if ($.isEmptyObject(data.error)) {
            $('.loader').hide();
            $('.success_msg').show();
          setTimeout(function(){
         window.location.reload();
}, 5000);
        } else {
           $('.loader').hide();
           
          printErrorMsg(data.error);


        }
      },

    });
    }else{
        $('.loader').hide();
        if (!$('#name').val()) {
                if ($("#name").parent().next(".validation").length == 0) // only add if not added
                {
                    $("#name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Name</div>");
                }
            } else {
                $("#name").parent().next(".validation").remove(); // remove it
            }
            if (!$('#title').val()) {
                if ($("#title").parent().next(".validation").length == 0) // only add if not added
                {
                    $("#title").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Title</div>");

                }
            } else {
                $("#title").parent().next(".validation").remove(); // remove it
            }
            
            if (!$('#rating').val()) {
                if ($("#rating").parent().next(".validation").length == 0) // only add if not added
                {
                    $("#rating").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Rating</div>");

                }
            } else {
                $("#rating").parent().next(".validation").remove(); // remove it
            }
            if (!$('#comments').val()) {
                if ($("#comments").parent().next(".validation").length == 0) // only add if not added
                {
                    $("#comments").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Please enter Comments</div>");

                }
            } else {
                $("#comments").parent().next(".validation").remove(); // remove it
            }
    }
  });


$(function () {
    $(".reviews").slice(1+1,1000).hide();
    //$(".reviews").children("div").slice(0, 2).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".reviews:hidden").slice(0, 2).slideDown();
        if ($(".reviews:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        /*$('html,body').animate({
           scrollTop: $(this).offset().top
        }, 6000);*/
    });
    
    // $('#VideoModal').on('show.bs.modal', function (event) {
    //     var button = $(event.relatedTarget);
    //     var imgposter = button.find("img").attr("src");
    //     var vidsrc = button.find("img").attr("data-src");

    //     //$(this).find('video#recap').attr('poster', imgposter);
    //     $(this).find('video#recap source').attr('src', '');
    //     $(this).find('video#recap source').attr('src', vidsrc);

    //     $(this).find('video#recap')[0].load();
    // }).on('hidden.bs.modal', function(){
    //     $(this).find('video')[0].pause();
    // });
});




 </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/frontend/inactive_product_view.blade.php ENDPATH**/ ?>