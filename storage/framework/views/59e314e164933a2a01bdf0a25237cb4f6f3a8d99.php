<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.show')); ?> <?php echo e(trans('cruds.product.title_singular')); ?>

        
       <a style="margin-top:5px; margin-right:10px; margin-left:10px;" class="btn btn-info float-right" href="<?php echo e(route('admin.products.edit', $product->id)); ?>">
                                   <?php echo e(trans('global.edit')); ?>

                                </a>
                                <a style="margin-top:5px;" class="btn btn-info float-right" href="<?php echo e(url('/admin/products')); ?>">
                <?php echo e(trans('global.back_to_list')); ?>

            </a>
    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.id')); ?>

                        </th>
                        <td>
                            <?php echo e($product->id); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.title')); ?>

                        </th>
                        <td>
                            <?php echo e($product->title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            Product Code
                        </th>
                        <td>
                            <?php echo e($product->sub_title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.category')); ?>

                        </th>
                        <td>
                            <?php echo e($product->category->title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.description')); ?>

                        </th>
                        <td>
                            <?php echo $product->description; ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.height')); ?>

                        </th>
                        <td>
                            <?php echo e($product->height); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.width')); ?>

                        </th>
                        <td>
                            <?php echo e($product->width); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.length')); ?>

                        </th>
                        <td>
                            <?php echo e($product->length); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.weight')); ?>

                        </th>
                        <td>
                            <?php echo e($product->weight); ?>

                        </td>
                    </tr>
                    <?php if($product->chain_length != ''): ?>
                        <tr>
                            <th>
                                <?php echo e(trans('cruds.product.fields.chain_length')); ?>

                            </th>
                            <td>
                                <?php echo e($product->chain_length); ?>

                            </td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.status')); ?>

                        </th>
                        <td>
                                <?php if($product->status == 1): ?>
                                <span class="badge badge-success">Active</span>
                                <?php else: ?>
                                <span class="badge badge-danger">Inactive</span>
                                <?php endif; ?>
   
                        </td>
                    </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.stock_available')); ?>

                        </th>
                        <td>
                                <?php if($product->status == 1): ?>
                                <span class="badge badge-success">Yes</span>
                                <?php else: ?>
                                <span class="badge badge-danger">No</span>
                                <?php endif; ?>
                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.delivery_days')); ?>

                        </th>
                        <td>
                            <?php echo e($product->delivery_days); ?>

                        </td>
                    </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.list_image')); ?>

                        </th>
                        <td>
                            <img class="img-height-view img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$product->list_image)); ?>" />
                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.images')); ?>

                        </th>
                        <td>
                            <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>    
                                <img class="img-height-view img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$image->image)); ?>" />
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </td>
                </tr>
                <!--<tr>-->
                <!--        <th>-->
                <!--            Videos-->
                <!--        </th>-->
                <!--        <td>-->
                <!--            <?php $__currentLoopData = $videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $video): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>-->
                <!--            <a href="#!" data-toggle="modal" data-target=".videopopup" >-->
                <!--                <img data-src="<?php echo e(url('storage/app/'.$video->video)); ?>" style="max-height:150px;" class="img-height-view img-fluid img-thumbnail" src="<?php echo e(url('/themes/play-button.png')); ?>" />-->
                <!--            </a>-->
                <!--            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>-->
                <!--        </td>-->
                <!--</tr>-->
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.meta_title')); ?>

                        </th>
                        <td>
                            <?php echo e($product->meta_title); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.meta_keywords')); ?>

                        </th>
                        <td>
                            <?php echo e($product->meta_keywords); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.product.fields.meta_description')); ?>

                        </th>
                        <td>
                            <?php echo e($product->meta_description); ?>

                        </td>
                </tr>
                </tbody>
            </table>
            
        </div>


    </div>
</div>
<div class="modal fade videopopup" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <button type="button" class="close videobtn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="modal-body">
            <video id="recap" width="100%" controls controlslist="nodownload" autoplay="">
            <source src="" type="video/mp4">
            <!-- <source src="" type="video/ogg"> -->
            Your browser does not support the video tag.
            </video>
        </div>
    </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var imgposter = button.find("img").attr("src");
        var vidsrc = button.find("img").attr("data-src");

        //$(this).find('video#recap').attr('poster', imgposter);
        $(this).find('video#recap source').attr('src', '');
        $(this).find('video#recap source').attr('src', vidsrc);

        $(this).find('video#recap')[0].load();
    }).on('hidden.bs.modal', function(){
        $(this).find('video')[0].pause();
    });
    

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/products/show.blade.php ENDPATH**/ ?>