<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.edit')); ?> <?php echo e(trans('cruds.page.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.pages.update", [$page->id])); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                            <label for="name"><?php echo e(trans('cruds.page.fields.title')); ?>*</label>
                            <input readonly="" type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($page) ? $page->title : '')); ?>" required>
                            <?php if($errors->has('title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.title_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('heading') ? 'has-error' : ''); ?>">
                            <label for="heading"><?php echo e(trans('cruds.page.fields.heading')); ?>*</label>
                            <input type="text" id="heading" name="heading" class="form-control" value="<?php echo e(old('heading', isset($page) ? $page->heading : '')); ?>" required>
                            <?php if($errors->has('heading')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('heading')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.heading_helper')); ?>

                            </p>
                        </div>
                    </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group <?php echo e($errors->has('content') ? 'has-error' : ''); ?>">
                        <label for="content"><?php echo e(trans('cruds.page.fields.content')); ?></label>
                        <textarea id="content" name="content" class="form-control ckeditor"><?php echo e(old('content', isset($page) ? $page->content : '')); ?></textarea>
                        <?php if($errors->has('content')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('content')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.page.fields.content_helper')); ?>

                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
                            <label for="image"><?php echo e(trans('cruds.page.fields.image')); ?>*</label>
                            <input type="file" id="image" name="image" class="form-control" >
                            <?php if($errors->has('image')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('image')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.image_helper')); ?>

                            </p>
                            <img class="img-height img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$page->image)); ?>" />
                        </div>    
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
                            <label for="status"><?php echo e(trans('cruds.page.fields.status')); ?>*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            <?php if($errors->has('status')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('status')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.status_helper')); ?>

                            </p>
                        </div>  
                    </div>
            </div>
            <?php if($page->slug == 'our-studio'): ?>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('youtube_id') ? 'has-error' : ''); ?>">
                            <label for="youtube_id"><?php echo e(trans('cruds.page.fields.youtube_id')); ?></label>
                            <input id="youtube_id" name="youtube_id" class="form-control" value="<?php echo e(old('youtube_id', isset($page) ? $page->youtube_id : '')); ?>">
                            <?php if($errors->has('youtube_id')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('youtube_id')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.youtube_id_helper')); ?>

                            </p>
                        </div>
                    </div>               
                </div>
            <?php endif; ?>
<!--            <?php if($page->slug == 'our-studio'): ?>
                <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('studio_images') ? 'has-error' : ''); ?>">
                            <label for="studio_images"><?php echo e(trans('cruds.page.fields.studio_images')); ?></label>
                            <input type="file" id="studio_images" name="studio_images[]" multiple class="form-control" >
                            <?php if($errors->has('studio_images')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('studio_images')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.studio_images_helper')); ?>

                            </p>
                            
                                </div>  
                        </div>
                </div>
                <div class="row mt-20">
                        <?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-lg-3">
                              <img class="img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$image->image)); ?>" />  
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php endif; ?>-->
            <h5 class="mb-20">SEO Content</h5>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('meta_title') ? 'has-error' : ''); ?>">
                            <label for="meta_title"><?php echo e(trans('cruds.page.fields.meta_title')); ?></label>
                            <input type="text" id="meta_title" name="meta_title" class="form-control"  value="<?php echo e(old('meta_title', isset($page) ? $page->meta_title : '')); ?>" >
                            <?php if($errors->has('meta_title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('meta_title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.page.fields.meta_title_helper')); ?>

                            </p>
                        </div>    
                    </div>
                    
            </div>
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('meta_keywords') ? 'has-error' : ''); ?>">
                        <label for="meta_keywords"><?php echo e(trans('cruds.page.fields.meta_keywords')); ?></label>
                        <textarea id="bottom_content" name="meta_keywords" class="form-control" ><?php echo e(old('meta_keywords', isset($page) ? $page->meta_keywords : '')); ?></textarea>
                        <?php if($errors->has('meta_keywords')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('meta_keywords')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.page.fields.meta_keywords_helper')); ?>

                        </p>
                    </div>
                    </div>
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('meta_description') ? 'has-error' : ''); ?>">
                        <label for="meta_description"><?php echo e(trans('cruds.page.fields.meta_description')); ?></label>
                        <textarea id="meta_description" name="meta_description" class="form-control" ><?php echo e(old('meta_description', isset($page) ? $page->meta_description : '')); ?></textarea>
                        <?php if($errors->has('meta_description')): ?>
                            <em class="invalid-feedback">
                                <?php echo e($errors->first('meta_description')); ?>

                            </em>
                        <?php endif; ?>
                        <p class="helper-block">
                            <?php echo e(trans('cruds.page.fields.meta_description_helper')); ?>

                        </p>
                    </div>
                    </div>
            </div>
            <div>
                <input class="btn btn-primary float-right" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/pages/edit.blade.php ENDPATH**/ ?>