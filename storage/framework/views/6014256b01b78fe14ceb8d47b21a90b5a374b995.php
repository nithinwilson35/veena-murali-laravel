<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.show')); ?> <?php echo e(trans('cruds.page.title_singular')); ?>

    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.id')); ?>

                        </th>
                        <td>
                            <?php echo e($page->id); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.title')); ?>

                        </th>
                        <td>
                            <?php echo e($page->title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.heading')); ?>

                        </th>
                        <td>
                            <?php echo e($page->heading); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.content')); ?>

                        </th>
                        <td>
                            <?php echo $page->content; ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.status')); ?>

                        </th>
                        <td>
                                <?php if($page->status == 1): ?>
                                <span class="badge badge-success">Active</span>
                                <?php else: ?>
                                <span class="badge badge-danger">Inactive</span>
                                <?php endif; ?>
   
                        </td>
                    </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.image')); ?>

                        </th>
                        <td>
                            <img class="img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$page->image)); ?>" />
                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.meta_title')); ?>

                        </th>
                        <td>
                            <?php echo e($page->meta_title); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.meta_keywords')); ?>

                        </th>
                        <td>
                            <?php echo e($page->meta_keywords); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.page.fields.meta_description')); ?>

                        </th>
                        <td>
                            <?php echo e($page->meta_description); ?>

                        </td>
                </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="<?php echo e(url()->previous()); ?>">
                <?php echo e(trans('global.back_to_list')); ?>

            </a>
        </div>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/pages/show.blade.php ENDPATH**/ ?>