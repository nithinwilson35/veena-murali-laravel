<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.show')); ?> <?php echo e(trans('cruds.category.title_singular')); ?>

    </div>

    <div class="card-body">
        <div class="mb-2">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.id')); ?>

                        </th>
                        <td>
                            <?php echo e($category->id); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.title')); ?>

                        </th>
                        <td>
                            <?php echo e($category->title); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.heading')); ?>

                        </th>
                        <td>
                            <?php echo e($category->heading); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.description')); ?>

                        </th>
                        <td>
                            <?php echo $category->description; ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.parent')); ?>

                        </th>
                        <td>
                            <?php echo e($category->heading); ?>

                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.status')); ?>

                        </th>
                        <td>
                                <?php if($category->status == 1): ?>
                                <span class="badge badge-success">Active</span>
                                <?php else: ?>
                                <span class="badge badge-danger">Inactive</span>
                                <?php endif; ?>
   
                        </td>
                    </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.bottom_content')); ?>

                        </th>
                        <td>
                            <?php echo e($category->bottom_content); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.banner_image')); ?>

                        </th>
                        <td>
                            <img class="img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$category->banner_image)); ?>" />
                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.meta_title')); ?>

                        </th>
                        <td>
                            <?php echo e($category->meta_title); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.meta_keywords')); ?>

                        </th>
                        <td>
                            <?php echo e($category->meta_keywords); ?>

                        </td>
                </tr>
                <tr>
                        <th>
                            <?php echo e(trans('cruds.category.fields.meta_description')); ?>

                        </th>
                        <td>
                            <?php echo e($category->meta_description); ?>

                        </td>
                </tr>
                </tbody>
            </table>
            <a style="margin-top:20px;" class="btn btn-info" href="<?php echo e(url()->previous()); ?>">
                <?php echo e(trans('global.back_to_list')); ?>

            </a>
        </div>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/categories/show.blade.php ENDPATH**/ ?>