<?php $__env->startSection('content'); ?>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.edit')); ?> <?php echo e(trans('cruds.banner.title_singular')); ?>

    </div>

    <div class="card-body">
        <form action="<?php echo e(route("admin.banners.update", [$banner->id])); ?>" method="POST" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <?php echo method_field('PUT'); ?>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('title') ? 'has-error' : ''); ?>">
                            <label for="name"><?php echo e(trans('cruds.banner.fields.title')); ?>*</label>
                            <input type="text" id="title" name="title" class="form-control" value="<?php echo e(old('title', isset($banner) ? $banner->title : '')); ?>" required>
                            <?php if($errors->has('title')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('title')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.banner.fields.title_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('status') ? 'has-error' : ''); ?>">
                            <label for="status"><?php echo e(trans('cruds.banner.fields.status')); ?>*</label>
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                            <?php if($errors->has('status')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('status')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.banner.fields.status_helper')); ?>

                            </p>
                        </div>  
                    </div>
            </div>
            <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('link_text') ? 'has-error' : ''); ?>">
                            <label for="link_text"><?php echo e(trans('cruds.banner.fields.link_text')); ?></label>
                            <input type="text" id="link_text" name="link_text" class="form-control" value="<?php echo e(old('link_text', isset($banner) ? $banner->link_text : '')); ?>">
                            <?php if($errors->has('heading')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('link_text')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.banner.fields.link_text_helper')); ?>

                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group <?php echo e($errors->has('link_url') ? 'has-error' : ''); ?>">
                            <label for="link_url"><?php echo e(trans('cruds.banner.fields.link_url')); ?></label>
                            <input type="text" id="link_url" name="link_url" class="form-control" value="<?php echo e(old('link_url', isset($banner) ? $banner->link_url : '')); ?>">
                            <?php if($errors->has('heading')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('link_url')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.banner.fields.link_url_helper')); ?>

                            </p>
                        </div>
                    </div>
                    
            </div>  
            <div class="row">
                    <div class="col-lg-6">
                            <div class="form-group <?php echo e($errors->has('image') ? 'has-error' : ''); ?>">
                            <label for="image"><?php echo e(trans('cruds.banner.fields.image')); ?></label>
                            <input type="file" id="image" name="image" class="form-control" >
                            <?php if($errors->has('image')): ?>
                                <em class="invalid-feedback">
                                    <?php echo e($errors->first('image')); ?>

                                </em>
                            <?php endif; ?>
                            <p class="helper-block">
                                <?php echo e(trans('cruds.banner.fields.image_helper')); ?>

                            </p>
                            <img class="img-fluid img-thumbnail" src="<?php echo e(url('storage/app/'.$banner->image)); ?>" />
                        </div>  
                    </div>
            </div>
            <div>
                <input class="btn btn-primary float-right" type="submit" value="<?php echo e(trans('global.save')); ?>">
            </div>
        </form>


    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/veenamur/public_html/resources/views/admin/banners/edit.blade.php ENDPATH**/ ?>