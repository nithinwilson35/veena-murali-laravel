<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'sub_title' => 'required',
            'category_id' => 'required',
            'height' => 'required',
            'width' => 'required',
            'weight' => 'required',
            'length' => 'required',
            'stock_available' => 'required',
            'delivery_days' => 'required',
            'status' => 'required',
            'list_image' => 'mimes:jpeg,png,jpg,gif,svg'
        ];
    }
}
