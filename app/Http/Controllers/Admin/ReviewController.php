<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Review;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ReviewController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $reviews = Review::orderBy('id', 'DESC')->get();

        return view('admin.review.index', compact('reviews'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  \App\Http\Requests\StorePagesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$user_id = '1';//Auth::user()->id;
        $review = new Review;
        $review->name                = $request->get('name');
        $review->title               = $request->get('title');
        $review->rating              = $request->get('rating');
        $review->comments            = $request->get('comments');
        //$review->a_id                = $user_id;
        $review->p_id                = $request->get('p_id');
        
       if ($request->hasFile('image')) {
        $array=array(); 
        foreach($request->file('image') as $image){
        $name = uniqid().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images/review_image');
        $imagePath = $destinationPath. "/".  $name;
        $image->move($destinationPath, $name);
        $array[]=$name;
       }
        $image_path = implode(",",$array);
        $review->image = $image_path;
      }
        $review->save();
        if($review){
           
            return response()->json(['success' => 'Added new review.']);
        }else{
            return response()->json(['error' => 'Review Added Failed.']);   
        }
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdatePagesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    public function show(Review $review)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.review.show', compact('review'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $review->delete();

        return redirect()->route('admin.review.index');
    }
    
    /**
     * Approve Review.
     *
     * @param  \App\Http\Requests\StorePagesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function review_approve(Request $request)
    {
        
        $review = DB::table('reviews')
            ->where('id',$request->get('id'))
            ->update(array('status' => 1));
        if($review){
           
            return response()->json(['success' => 'Review Approved.']);
        }else{
            return response()->json(['error' => 'Review Approve Failed.']);   
        }
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        Review::whereIn('id', request('ids'))->delete();

        return response(null);
    }
    
    
    
}
