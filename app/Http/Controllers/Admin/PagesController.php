<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\Studio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePagesRequest;
use App\Http\Requests\Admin\UpdatePagesRequest;
use Illuminate\Support\Str;

class PagesController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $pages = Page::all();

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.pages.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  \App\Http\Requests\StorePagesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePagesRequest $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $input = $request->except('_token');
        
        $input['slug'] = Str::slug($input['title'], '-');

        if ($request->hasFile('image')) {
            $input['image'] = $this->doUpload($input);
        } else {
            unset($input['image']);
        }
        
//        if ($request->hasFile('youtube_id')) {
//            $input['youtube_id'] = $this->doVideoUpload($input);
//        } else {
//            unset($input['youtube_id']);
//        }
        
        $page = Page::create($input);
        
        return redirect()->route('admin.pages.index')->with('message', 'Page '.$page->title.' created successfully.');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $images = collect();
        if ($page->slug == 'our-studio') {
                $images = Studio::all();
        }
          
        return view('admin.pages.edit', compact('page', 'images'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdatePagesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePagesRequest $request, Page $page)
    {
        $files = [];
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $input = $request->except('_token');
        
        if ($request->hasFile('image')) {
            $input['image'] = $this->doUpload($input);
        } else {
            unset($input['image']);
        }
        
//        if ($request->hasFile('youtube_id')) {
//            $input['youtube_id'] = $this->doVideoUpload($input);
//        } else {
//            unset($input['youtube_id']);
//        }
        
        if ($request->hasFile('studio_images')) {
                $files = $this->studioImagesUpload($input);
                unset($input['studio_images']);
                foreach ($files as $file) {
                        Studio::create([
                            'image' => $file
                        ]);
                }
        }
        
        $page->update($input);
        return redirect()->route('admin.pages.index')->with('message', 'Page '.$page->title.' updated successfully.');
    }

    public function show(Page $page)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.pages.show', compact('page'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $page->delete();

        return redirect()->route('admin.pages.index')->with('message', 'Product deleted successfully.');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        Page::whereIn('id', request('ids'))->delete();

        return response(null);
    }
    
    public function doUpload(array $request)
    {
        $image = $request['image'];
        $imageName = time().$image->getClientOriginalName();
        $file = $image->storeAs('pages', $imageName);
        return $file;
    }
    
    public function studioImagesUpload(array $request)
    {
        $images = $request['studio_images'];
        $files = array();
        foreach ($images as $image) {
            $imageName = time().$image->getClientOriginalName();
            $files[] = $image->storeAs('studios', $imageName);
        }
        return $files;
    }
    
    public function doVideoUpload(array $request)
    {
        $image = $request['youtube_id'];
        $imageName = time().$image->getClientOriginalName();
        $file = $image->storeAs('pages/videos', $imageName);
        return $file;
    }

}
