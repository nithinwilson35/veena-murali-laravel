<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCategoriesRequest;
use App\Http\Requests\Admin\UpdateCategoriesRequest;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $parents = Category::where('parent_id', 0)->get();

        return view('admin.categories.create', compact('parents'));
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  \App\Http\Requests\StoreCategoriesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoriesRequest $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $input = $request->except('_token');
        
        $input['slug'] = Str::slug($input['title'], '-');

        if ($request->hasFile('banner_image')) {
            $input['banner_image'] = $this->doUpload($input);
        } else {
            unset($input['banner_image']);
        }
        
        $category = Category::create($input);
        
        return redirect()->route('admin.categories.index')->with('message', 'Category '.$category->title.' created successfully.');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $parents = Category::where('parent_id', 0)->where('id', '!=', $category->id)->get();
        
        return view('admin.categories.edit', compact('category', 'parents'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateCategoriesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoriesRequest $request, Category $category)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $input = $request->except('_token');
        
        $input['slug'] = Str::slug($input['title'], '-');

        if ($request->hasFile('banner_image')) {
            $input['banner_image'] = $this->doUpload($input);
        } else {
            unset($input['banner_image']);
        }

        $category->update($input);
        return redirect()->route('admin.categories.index')->with('message', 'Category '.$category->title.' updated successfully.');
    }

    public function show(Category $category)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.categories.show', compact('category'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $category->delete();

        return redirect()->route('admin.categories.index')->with('message', 'Category deleted successfully.');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        Category::whereIn('id', request('ids'))->delete();

        return response(null);
    }
    
    public function doUpload(array $request)
    {
        $image = $request['banner_image'];
        $imageName = time().$image->getClientOriginalName();
        $file = $image->storeAs('categories', $imageName);
        return $file;
    }

}
