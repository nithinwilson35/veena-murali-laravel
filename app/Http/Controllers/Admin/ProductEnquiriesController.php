<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProductEnquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class ProductEnquiriesController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $enquiries = ProductEnquiry::with('product')->get();

        return view('admin.product_enquiries.index', compact('enquiries'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  \App\Http\Requests\StorePagesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdatePagesRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    public function show(ProductEnquiry $product_enquiry)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        return view('admin.product_enquiries.show', compact('product_enquiry'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductEnquiry $product_enquiry)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $product_enquiry->delete();

        return redirect()->route('admin.product-enquiries.index');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        ProductEnquiry::whereIn('id', request('ids'))->delete();

        return response(null);
    }
    
}
