<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreBannersRequest;
use App\Http\Requests\Admin\UpdateBannersRequest;
use Illuminate\Support\Str;

class BannersController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $banners = Banner::all();

        return view('admin.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.banners.create');
    }

    /**
     * Store a newly created BAnner in storage.
     *
     * @param  \App\Http\Requests\StoreBannersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBannersRequest $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $input = $request->except('_token');
        
        if ($request->hasFile('image')) {
            $input['image'] = $this->doUpload($input);
        } else {
            unset($input['image']);
        }
        
        $banner = Banner::create($input);
        
        return redirect()->route('admin.banners.index')->with('message', 'Banner '.$banner->title.' created successfully.');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        return view('admin.banners.edit', compact('banner'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateBannersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBannersRequest $request, Banner $banner)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $input = $request->except('_token');
         

        if ($request->hasFile('image')) {
            $input['image'] = $this->doUpload($input);
        } else {
            unset($input['image']);
        }

        $banner->update($input);
        return redirect()->route('admin.banners.index')->with('message', 'Banner '.$banner->title.' updated successfully.');
    }

    public function show(Banner $banner)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.banners.show', compact('banner'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $banner->delete();

        return redirect()->route('admin.banners.index')->with('message', 'Banner deleted successfully.');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        Banner::whereIn('id', request('ids'))->delete();

        return response(null);
    }
    
    public function doUpload(array $request)
    {
        $image = $request['image'];
        $imageName = time().$image->getClientOriginalName();
        $file = $image->storeAs('banners', $imageName);
        return $file;
    }

}
