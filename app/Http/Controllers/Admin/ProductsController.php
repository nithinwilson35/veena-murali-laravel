<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreProductsRequest;
use App\Http\Requests\Admin\UpdateProductsRequest;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $products = Product::orderByDesc('id')->get();

        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $parents = Category::where('parent_id', 0)->get();
        $childrens = Category::where('parent_id', '!=', 0)->get();
        
        return view('admin.products.create', compact('parents', 'childrens'));
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  \App\Http\Requests\StoreProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductsRequest $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        $input = $request->except('_token');
        $videos = array();
        
        $input['slug'] = Str::slug($input['title'], '-');

        if ($request->hasFile('list_image')) {
            $input['list_image'] = $this->doUpload($input);
        } else {
            unset($input['list_image']);
        }
        
        $files = $this->multipleUpload($input);
        unset($input['images']);
        
        if ($request->has('videos')) {
            $videos = explode(',', $request->videos);
        } 
        unset($input['videos']);
        
        $latest = Product::orderByDesc('id')->first()->sub_title ?? 0;
        
        $latest = preg_replace("/[^0-9\.]/", '', $latest);
                
        $latest = "SKU VM". sprintf('%04d', $latest+1);
        
        $input['sub_title'] = $latest;
        
        
        $product = Product::create($input);
        
        foreach ($files as $file) {
            ProductImage::create([
                'product_id' => $product->id,
                'image' => $file
            ]);
        }
        
        foreach (array_filter($videos) as $file1) {
            ProductVideo::create([
                'product_id' => $product->id,
                'video' => $file1
            ]);
        }
        
        return redirect()->route('admin.products.index')->with('message', 'Product '.$product->title.' created successfully.');
    }


    /**
     * Show the form for editing User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $parents = Category::where('parent_id', 0)->get();
        $childrens = Category::where('parent_id', '!=', 0)->get();

        $images = ProductImage::where('product_id', $product->id)->get();
        $videos = ProductVideo::where('product_id', $product->id)->get();
        return view('admin.products.edit', compact('product', 'parents', 'images', 'childrens', 'videos'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateProductsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductsRequest $request, Product $product)
    {
        $files = array();
        $videos = array();
        
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $input = $request->except('_token');
        
        $input['slug'] = Str::slug($input['title'], '-');

        if ($request->hasFile('list_image')) {
            $input['list_image'] = $this->doUpload($input);
        } else {
            unset($input['list_image']);
        }
        
        if ($request->hasFile('images')) {
                $files = $this->multipleUpload($input);
        }
        unset($input['images']);
        
        if ($request->has('videos')) {
            $videos = explode(',', $request->videos);
        } 
        unset($input['videos']);

        $product->update($input);
        
        foreach ($files as $file) {
            ProductImage::create([
                'product_id' => $product->id,
                'image' => $file
            ]);
        }
        
        ProductVideo::where('product_id', $product->id)->delete();
        
        foreach (array_filter($videos) as $file) {
            ProductVideo::create([
                'product_id' => $product->id,
                'video' => $file
            ]);
        }
        
        return redirect()->route('admin.products.index')->with('message', 'Product '.$product->title.' updated successfully.');
    }

    public function show(Product $product)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        
        $images = ProductImage::where('product_id', $product->id)->get();
        $videos = ProductVideo::where('product_id', $product->id)->get();

        return view('admin.products.show', compact('product', 'images', 'videos'));
    }

    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }

        $product->delete();

        return redirect()->route('admin.products.index')->with('message', 'Product deleted successfully.');
    }

    /**
     * Delete all selected User at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('users_manage')) {
            return abort(401);
        }
        Product::whereIn('id', request('ids'))->delete();

        return response(null);
    }
    
    public function doUpload(array $request)
    {
        $image = $request['list_image'];
        $imageName = time().$image->getClientOriginalName();
        $file = $image->storeAs('products/list', $imageName);
        return $file;
    }
    
    public function multipleUpload(array $request)
    {
        $images = $request['images'];
        $files = array();
        foreach ($images as $image) {
            $imageName = time().$image->getClientOriginalName();
            $files[] = $image->storeAs('products/images', $imageName);
        }
        return $files;
    }
    
    public function multipleVideoUpload(array $request)
    {
        $videos = $request['videos'];
        $files = array();
        foreach ($videos as $video) {
            $videoName = time().$video->getClientOriginalName();
            $files[] = $video->storeAs('products/videos', $videoName);
        }
        return $files;
    }
    
    
    public function deleteImage($id)
    {
        $image = ProductImage::find($id);
        if ($image) {
            $image->delete();
        }
        
        return response()->json([
            'status' => 1,
            'msg' => 'Image deleted successfully.'
        ]);
    }
    
    public function deleteVideo($id)
    {
        $video = ProductVideo::find($id);
        if ($video) {
            $video->delete();
        }
        
        return response()->json([
            'status' => 1,
            'msg' => 'Video deleted successfully.'
        ]);
    }
    
    public function massUpdate()
    {
        $products = Product::all();
        
        $latest = Product::orderByDesc('id')->first()->sub_title ?? 0;
        
        dd($latest);
        
        foreach ($products as $key => $product) {
            
            if ($key == 0) {
                $id = "SKU VM0001";
            } else {
                $id = preg_replace("/[^0-9\.]/", '', $id);
                
                $id = "SKU VM". sprintf('%04d', $id+1);
            }
            echo $product->title."---".$id."</br>";
            
            $product->sub_title = $id;
            
            $product->save();
        }
    }
}
