<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Enquiry;
use App\Models\ProductEnquiry;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\Product;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seo = Page::where('slug', 'contact-us')->first();
        return view('frontend.contact', compact('seo'));
    }
    
    public function sendMessage(Request $request)
    {
        $mailContent = $request->all();
        
        if (isset($mailContent['product_id'])) {
             ProductEnquiry::create($mailContent);
             $mailContent['product'] = Product::find($mailContent['product_id'])->title;
             unset($mailContent['product_id']);
             
        } else {
                Enquiry::create($mailContent);
        }

        $subject = "New Enquiry in Veena Murali Decors";
        Mail::send('emails.contact', ['content' => $mailContent], function($message) use ($subject,$mailContent) {
            // note: if you don't set this, it will use the defaults from config/mail.php
            $message->from('no-reply@veenamuralidecors.com', 'Veena Murali Decors');
                $message->to('veenamuralidecors2017@gmail.com', 'Veena Murali')
              ->subject($subject);
        });

        return response()->json([
            'status' => 1,
            'msg' => 'Message Submitted Successfully.'
        ]);
    }
}
