<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Studio;
use App\Models\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::active()->get();
        $ourStory = Page::where('slug', 'our-story')->first();
        $ourCulture = Page::where('slug', 'our-culture')->first();
        $ourStudio = Page::where('slug', 'our-studio')->first();
        $theBest = Page::where('slug', 'the-best-and-latest')->first();
        $seo = Page::where('slug', 'home')->first();
        $products = Product::with('category')->active()->latest()->limit('6')->get();
        $studios = Studio::all();
        return view('frontend.home', compact('banners', 'ourStory', 'ourCulture', 'ourStudio', 'theBest', 'seo', 'products', 'studios'));
    }
}
