<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Page;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function returnPolicy()
    {
        $seo = Page::where('slug', 'return-policy')->first();
        return view('frontend.return', compact('seo'));
    }
    
    public function faq()
    {
        $seo = Page::where('slug', 'faq')->first();
        return view('frontend.faq', compact('seo'));
    }
}
