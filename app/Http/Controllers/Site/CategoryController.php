<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Review;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $seo = Category::where('slug', $slug)->first();
        if ($seo) {
            $products = Product::with('category')->active()->where(function($query) use($seo){ $query->where('category_id', $seo->id)->orWhere('subcategory_id', $seo->id);})->get();
            $whatsappText = "Hi I am interested in ".$seo->title."%0A".url('/'.$seo->slug)."%0APlease Provide Details.";
            return view('frontend.product_list', compact('seo', 'products', 'whatsappText'));    
        }
        
        $seo = Product::with('category')->with('productImages')->with('productVideos')->where('slug', $slug)->first();
        if ($seo) {
            $category = Category::where('id', $seo->category_id)->first();
            if (!$seo->status) {
                return view('frontend.inactive_product_view', compact('category'));
            }
            
            $products = Product::with('category')->latest()->active()->where('category_id', $seo->category_id)->where('id', '!=', $seo->id)->limit(8)->get();
            $whatsappText = "Hi I am interested in ".$seo->title."%0A".url('/'.$seo->slug)."%0APlease Provide Details.";
            $reviews = Review::where('status', '1')->orderBy('id', 'DESC')->get();
            return view('frontend.product_view', compact('seo', 'products', 'category', 'whatsappText','reviews'));    
        }
        
        abort(404);
    }
}
