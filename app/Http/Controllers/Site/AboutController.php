<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Studio;

class AboutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seo = Page::where('slug', 'about-us')->first();
        $ourStory = Page::where('slug', 'our-story')->first();
        $ourCulture = Page::where('slug', 'our-culture')->first();
        $ourStudio = Page::where('slug', 'our-studio')->first();
        $studios = Studio::all();
        return view('frontend.about', compact('seo', 'ourStory', 'ourCulture', 'ourStudio', 'studios'));
    }
}
