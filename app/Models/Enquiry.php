<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Enquiry
 *
*/
class Enquiry extends Model
{
    protected $fillable = ['name','email','phone', 'message'];
    
}
