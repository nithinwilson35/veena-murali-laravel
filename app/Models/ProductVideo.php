<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductVideo
 *
*/
class ProductVideo extends Model
{
    protected $fillable = ['product_id','video'];
}
