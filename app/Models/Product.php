<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class Product extends Model
{ 
    protected $fillable = ['title', 'description', 'slug', 'sub_title', 'status', 'category_id', 'subcategory_id', 'height', 'width', 'weight', 'length', 'chain_length', 'stock_available', 'delivery_days', 'list_image', 'meta_title', 'meta_keywords', 'meta_description'];
    
    public function category() {
        return $this->belongsTo('App\Models\Category');
    }
    
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
    
    public function productImages()
    {
        return $this->hasMany('App\Models\ProductImage');
    }
    
    public function productVideos()
    {
        return $this->hasMany('App\Models\ProductVideo');
    }
}
