<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 *
*/
class Review extends Model
{
    protected $fillable = ['name','email','title', 'comments','image'];
    
}
