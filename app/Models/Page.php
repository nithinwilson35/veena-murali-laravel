<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class Page extends Model
{ 
    protected $fillable = ['title', 'content', 'slug', 'heading', 'status', 'image', 'meta_title', 'meta_keywords', 'meta_description','youtube_id'];
}
