<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class Category extends Model
{ 
    protected $fillable = ['title', 'description', 'slug', 'heading', 'status', 'parent_id', 'bottom_content', 'banner_image', 'meta_title', 'meta_keywords', 'meta_description'];
    
        /**
        * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
        */
        public function parent() {
            return $this->belongsTo(self::class, 'parent_id');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function children() {
            return $this->hasMany(self::class, 'parent_id','id')->where('status', 1);
        }
        
        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }
        
        public function scopeParents($query)
        {
            return $query->where('parent_id', 0);
        }
}
