<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductImage
 *
*/
class ProductImage extends Model
{
    protected $fillable = ['product_id','image'];
}
