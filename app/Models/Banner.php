<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Banner
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class Banner extends Model
{ 
    protected $fillable = ['title', 'link_text', 'link_url', 'image', 'status'];
    
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
