<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Studio
 *
*/
class Studio extends Model
{
    protected $fillable = ['image'];
}
