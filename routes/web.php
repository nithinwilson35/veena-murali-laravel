<?php

Auth::routes(['register' => false]);

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');
Route::post('review/store', 'Admin\ReviewController@store')->name('review.store');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');
    Route::resource('categories', 'Admin\CategoriesController');
    Route::post('categories_mass_destroy', 'Admin\CategoriesController@massDestroy')->name('categories.mass_destroy');
    Route::resource('banners', 'Admin\BannersController');
    Route::post('banners_mass_destroy', 'Admin\BannersController@massDestroy')->name('banners.mass_destroy');
    Route::resource('pages', 'Admin\PagesController');
    Route::post('pages_mass_destroy', 'Admin\PagesController@massDestroy')->name('pages.mass_destroy');
    Route::resource('products', 'Admin\ProductsController');
    Route::post('products_mass_destroy', 'Admin\ProductsController@massDestroy')->name('products.mass_destroy');
    Route::get('products-mass-update', 'Admin\ProductsController@massUpdate')->name('products.mass_update');
    Route::resource('enquiries', 'Admin\EnquiriesController');
    Route::post('enquiries_mass_destroy', 'Admin\EnquiriesController@massDestroy')->name('enquiries.mass_destroy');
    Route::resource('review', 'Admin\ReviewController');
    Route::post('review/approve', 'Admin\ReviewController@review_approve')->name('review.approve');
    Route::post('review_mass_destroy', 'Admin\ReviewController@massDestroy')->name('review.mass_destroy');
    Route::resource('product-enquiries', 'Admin\ProductEnquiriesController');
    Route::post('product_enquiries_mass_destroy', 'Admin\ProductEnquiriesController@massDestroy')->name('product_enquiries.mass_destroy');
    Route::get('/products/delete-image/{id}', 'Admin\ProductsController@deleteImage')->name('delete_image');
    Route::get('/products/delete-video/{id}', 'Admin\ProductsController@deleteVideo')->name('delete_video');
});

Route::get('/', 'Site\HomeController@index')->name('home');
Route::get('/about-us', 'Site\AboutController@index')->name('about');
Route::get('/contact-us', 'Site\ContactController@index')->name('contact-us');
Route::get('/return-policy', 'Site\PageController@returnPolicy')->name('return-policy');
Route::get('/faq', 'Site\PageController@faq')->name('faq');
Route::get('/{slug}', 'Site\CategoryController@index')->name('category');
Route::post('/contact-us', 'Site\ContactController@sendMessage')->name('contact-submit');

