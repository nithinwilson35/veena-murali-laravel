//  banner slider start
$('.banner-slider').slick({
    autoplay: true,
    pauseOnHover: false,
    autoplaySpeed: 2000,
    draggable: true,
    dots: true,
    arrows: false,
    infinite: true,
    fade: true
});
$('.pd-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  //asNavFor: '.pd-slider-nav',
  autoplay: true,
  autoplaySpeed: 2500,
  pauseOnHover: true 
});
$('.pd-slider-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.pd-slider',
  dots: true,
  centerMode: true,
  focusOnSelect: true,
  infinite: false
});

$('.change-slick').click(function() {
	$('.pd-slider').slick('goTo',$(this).data('slick'));
})

// $('.pd-slider').slick({
//     dots: true,
//     arrows: false,
//     infinite: true,
//     speed: 500,
//     fade: true,
//     cssEase: 'linear',
//     autoPlay: true
// });
//  banner slider end
//  home product slider start
$('.pdt-slider').slick({
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    draggable: false,
    pauseOnHover: false
});
$('.pdt-slider2').slick({
    // centerPadding: '60px',
    slidesToShow: 3,
    initialSlide: 1,
    autoplay: true,
    pauseOnHover: false,
    autoplaySpeed: 2000,
    draggable: false,
    arrows: false,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
          }
        }
      ]
});


$('.about-slide').slick({
    slidesToShow: 3,
    infinite: true,
    autoplay: true,
    pauseOnHover: false,
    autoplaySpeed: 2000,
    draggable: true,
    arrows: false,
    responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$('.about-slide').slick('setPosition');
	});

//  home product slider end